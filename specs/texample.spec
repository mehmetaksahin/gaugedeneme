Specification Heading
=====================
Created by sahabt on 2019-05-17

This is an executable specification file which follows markdown syntax.
Every heading in this file denotes a scenario. Every bulleted point denotes a step.
     
Scenario Scenario1
-------------
Tags:test2
* Go to "http://www.milliyet.com.tr/"
* Find twitter button
* Wait "500" ms

Scenario TelekomSorguSenaryolariv
-------------

Tags:TelekomSorguSenaryolari-v
* Go to "http://www.milliyet.com.tr/"
* Wait "3000" ms

Scenario TelekomSorguSenaryolariv1
-------------

Tags:TelekomSorguSenaryolari-v1
* Go to "http://www.milliyet.com.tr/"
* Wait "3000" ms

Scenario TelekomSorguSenaryolariv2
-------------

Tags:TelekomSorguSenaryolari-v2
* Go to "deneme1 deneme2 deneme3 deneme4 deneme5 segeli deneme1 deneme2 deneme3 deneme4 deneme5 segeli2"
* Wait "2000" ms